import asyncio
import discord
from discord.ext import commands
import mysql.connector
from mysql.connector import errorcode
from PIL import Image, ImageFont, ImageDraw
import aiohttp
import os


class MEME():

    def __init__(self, client):
        self.client = client

    @commands.command(pass_context = True)
    async def meme(self, ctx, *args):

        try:
            db_co = mysql.connector.connect(user= 'Arcarox_querry', host = '86.74.198.79', port = '3306', password = 'arcarox', database = 'klein_bot')
            db_cursor = db_co.cursor()

        except mysql.connector.Error as err :

            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Something is wrong with your user name or password")

            elif err.errno == errorcode.ER_BAD_DB_ERROR:

                print("Database does not exist")

            else:
                print(err)

        cmd = args[0]


        if cmd == "view" or cmd == "v" :
            await self.client.say("Recherche")
            meme_name = args[1]
            db_cursor.execute('SELECT link FROM meme_links WHERE name = "' + meme_name + '"')

            db_output = db_cursor.fetchall()

            link = str(db_output)
            link = link.replace('"','')
            link = link.replace("'","")
            link = link.replace("(","")
            link = link.replace(')','')
            link = link.replace(',',"")
            link = link.replace('[',"")
            link = link.replace(']',"")

            try:
                await self.client.send_file(ctx.message.channel, link)

            except Exeption as e:
                print(e)
                try:
                    await self.client.say(link)

                except Exception as e:
                    print(e)
                    await self.client.say("Aucun fichier trouvé")


        if cmd == "add" :

            try:
                name = args[1]

            except:
                await self.client.say("Manque nom du meme")
                return

            db_cursor.execute('SELECT name FROM meme_links WHERE name = "' + name + '"')

            db_output = db_cursor.fetchall()

            if name not in str(db_output) :

                try:
                    async with aiohttp.ClientSession() as session:
                        async with session.get((ctx.message.attachments[0]['url'])) as r:
                            filename = ctx.message.attachments[0]['filename']

                            while filename[0] != '.':

                                filename = filename[1:]
                            filetype = filename
                            link = '../meme_gif/' + name + filetype
                            with open(link , "wb") as file:
                                buffer = await r.read()
                                file.write(buffer)
                                file.close()

                                querry = 'INSERT INTO meme_links (name, link) VALUES ("' + name + '", "' + link + '");'
                                db_cursor.execute(querry)
                                db_co.commit()
                                await self.client.say("Fait")

                except:
                    await asyncio.sleep(1)
                    # await self.client.say(ctx.message.embeds)
                    link = ctx.message.embeds[0]['url']

                    query = 'INSERT INTO meme_links (name, link) VALUES ("' + name + '", "' + link + '");'
                    db_cursor.execute(query)
                    db_co.commit()
                    await self.client.say("Fait")

            else :
                await self.client.say("Un meme avec le même nom existe déjà")


        if cmd == "remove" or cmd == "r":

            name = args[1]

            db_cursor.execute('SELECT link FROM meme_links WHERE name = "' + name + '"')

            db_output = db_cursor.fetchall()

            link = str(db_output)
            link = link.replace('"','')
            link = link.replace("'","")
            link = link.replace("(","")
            link = link.replace(')','')
            link = link.replace(',',"")
            link = link.replace('[',"")
            link = link.replace(']',"")
            link = link.replace(',',"")

            try:
                os.remove(link)
                await self.client.say("Fait")

            except:

                await self.client.say("Ce meme n'existe pas")

            query = 'DELETE FROM meme_links WHERE name = "' + name + '";'
            db_cursor.execute(query)
            db_co.commit()
            db_cursor.close()

        if cmd == "list" or cmd == "l":

            db_cursor.execute('SELECT name FROM meme_links')

            db_output = db_cursor.fetchall()

            list_name = []

            for i in range (0, len(db_output)):

                name = str(db_output[i])

                name = name.replace("'","")
                name = name.replace("(","")
                name = name.replace(")","")
                name = name.replace(",","")
                name = name.replace("[","")
                name = name.replace('"','')
                name = name.replace("]","")

                list_name.append(name)

            else:
                try:
                    p = int(args[1])

                except:
                    p = 1

            min = int(p * 10 - 10)
            max = int(p* 10)

            if len(list_name) % 10 == 0:
                pmax = int(len(list_name) / 10)

            else:
                pmax = int(len(list_name) / 10) + 1

            list_name = list_name[min: max]
            name_t = ''
            embed = discord.Embed(title ="Liste des noms des memes", description = 'Les noms des meme pour les voirs avec !k meme view')

            for i in range (0, len(list_name)):

                name_t = name_t + list_name[i]

                embed.add_field(name = list_name[i], value = 'to view : !k meme v ' + list_name[i], inline = False)

            embed.set_footer(text = 'Page : ' + str(p) + ' / ' + str(pmax))
            await self.client.say(embed = embed)

        
        if cmd == 'create' or cmd == 'new':

            unaccepted_filetype = ['.gif',]

            try:
                async with aiohttp.ClientSession() as session:
                    async with session.get((ctx.message.attachments[0]['url'])) as r:
                        filename = ctx.message.attachments[0]['filename']

                        while filename[0] != '.':

                            filename = filename[1:]
                        filetype = filename

                        if filetype in unaccepted_filetype:
                            await self.client.say("L'extension du fichier n'est pas compatible")
                            return

                        with open('../create_meme/created' + filetype , "wb") as file:
                                    buffer = await r.read()
                                    file.write(buffer)
                                    file.close()

            except:
                await self.client.say("Pas d'image trouvée")
                return

            im = Image.open('../create_meme/created' + filetype)

            L, H = im.size

            text1 = ''
            text2 = ''

            try:
                text =  ''.join(item + ' ' for item in args[1:])

            except:
                await self.client.say("Aucune phrase trouvée")
                return

            if '|' in text:
                index = text.index('|')

                try:
                    text1 = text[0:index]
                    text2 = text[index + 1:]

                except:
                    text1 = ''

            if im.mode == 'RGB':

                draw = ImageDraw.Draw(im)

                font_size = 64

                font = ImageFont.truetype("../Font/impact.ttf", font_size)

                if text1 == '':

                    l, h = font.getsize(text)

                    while l >= L * 0.9:
                        font_size -= 2

                        if font_size < 8:
                            await self.client.say("Phrase trop longue")
                            return

                        font = ImageFont.truetype("../Font/impact.ttf", font_size)
                        l, h = font.getsize(text)

                    draw.text(((L - l) / 2, (H - h) / 20), text, (0, 0, 0), font = font)

                else:

                    l, h = font.getsize(text1)

                    while l >= L * 0.9:
                        font_size -= 2

                        if font_size < 8:
                            await self.client.say("Phrase trop longue")
                            return
                            
                        font = ImageFont.truetype("../Font/impact.ttf", font_size)
                        l, h = font.getsize(text1)

                    draw.text(((L - l) / 2, (H - h) / 20), text1, (0, 0, 0), font = font)

                    font_size = 64

                    l, h = font.getsize(text2)

                    while l >= L * 0.9:
                        font_size -= 8

                        if font_size < 8:
                            await self.client.say("Phrase trop longue")
                            return
                    
                            
                        font = ImageFont.truetype("../Font/impact.ttf", font_size)
                        l, h = font.getsize(text2)

                    draw.text(((L - l) / 2, ((H - h) / 20) * 19), text2, (0, 0, 0), font = font)


                im.save('../create_meme/created' + filename)
                await self.client.send_file(ctx.message.channel, '../create_meme/created' + filename)
            
            else:
                im = im.convert('RGB')

                draw = ImageDraw.Draw(im)
                font = ImageFont.truetype("../Font/impact.ttf", 64)

                if text1 == '':

                    l, h = font.getsize(text)
                    draw.text(((L - l) / 2, (H - h) / 0.05), text, (0, 0, 0), font = font)

                else:
                    l, h = font.getsize(text1)
                    draw.text(((L - l) / 2, (H - h) * 0.05), text1, (0, 0, 0), font = font)

                    l, h = font.getsize(text2)
                    draw.text(((L - l) / 2, (H - h) * 0.95), text2, (0, 0, 0), font = font)

                im.save('../create_meme/created.jpg')
                await self.client.send_file(ctx.message.channel, '../create_meme/created.jpg')


        if cmd == "help" or cmd == "h":

            embed = discord.Embed(title = 'help pour meme', descritpion = 'commande possible pour meme', inline = False)
            embed.add_field(name= "!k meme l ou !k meme list {page}", value = "list des noms des memes", inline = False)
            embed.add_field(name= "!k meme v ou !k meme view {nom du meme}", value = "voir un meme avec le nom", inline = False)
            embed.add_field(name= "!k meme add {nom du meme} {fichier joint}", value = "Enregistre un meme pour plus pouvoir le revoir", inline = False)
            embed.add_field(name= "!k meme remove ou  !k memme r  {nom du name}", value= "supprimer un meme de la base de donnees", inline = False)
            embed.add_field(name= "!k meme create { text } optionnelle + | { text2 } + image", value= "Créer un meme en ajoutant du texte sur une image")

            await self.client.say(embed = embed)

        db_co.close()

def setup(client):

    client.add_cog(MEME(client))
