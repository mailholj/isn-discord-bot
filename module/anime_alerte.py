import asyncio
import discord
from discord.ext import commands
import mysql.connector
from mysql.connector import errorcode
import datetime
import re

class AL ():

    def __init__(self, client):

        self.client = client


    async def on_server_join(self, server):

        try:
            db_co = mysql.connector.connect(user= 'Arcarox_querry', host = '86.74.198.79', port = '3306', password = 'arcarox', database = 'klein_bot')
            db_cursor = db_co.cursor()

        except mysql.connector.Error as err :

            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Something is wrong with your user name or password")

            elif err.errno == errorcode.ER_BAD_DB_ERROR:

                print("Database does not exist")

            else:
                print(err)

        server_id = str(server.id)

        query = 'SELECT server FROM AL_server WHERE server = "' + server_id + '"'
        db_cursor.execute(query)
        db_output = db_cursor.fetchall()

        if server not in db_output:

            query = 'INSERT INTO AL_server (server, as_to_be_display) VALUES ( "' + server_id + '", "False")'

            db_cursor.execute(query)
            db_co.commit()
            db_co.close()


    async def on_server_remove(self, server):

        try:
            db_co = mysql.connector.connect(user= 'Arcarox_querry', host = '86.74.198.79', port = '3306', password = 'arcarox', database = 'klein_bot')
            db_cursor = db_co.cursor()

        except mysql.connector.Error as err :

            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Something is wrong with your user name or password")

            elif err.errno == errorcode.ER_BAD_DB_ERROR:

                print("Database does not exist")

            else:
                print(err)

        server_id = str(server.id)

        query = 'DELETE FROM AL_server WHERE server = "' + server_id + '"' 

        db_cursor.execute(query)
        db_co.commit()
        db_co.close()


    @commands.command(pass_context = True)
    async def AL_conf(self, ctx, *args):

        try :
            cmd = args[0]

        except:
            cmd = 'help'

        try:
            db_co = mysql.connector.connect(user= 'Arcarox_querry', host = '86.74.198.79', port = '3306', password = 'arcarox', database = 'klein_bot')
            db_cursor = db_co.cursor()

        except mysql.connector.Error as err :

            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Something is wrong with your user name or password")

            elif err.errno == errorcode.ER_BAD_DB_ERROR:

                print("Database does not exist")

            else:
                print(err)

        query = '''CREATE TABLE IF NOT EXISTS AL (
            id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
            name VARCHAR(255) NOT NULL,
            day VARCHAR(10) NOT NULL,
            hour CHAR(16) NOT NULL
            );'''

        db_cursor.execute(query)
        db_co.commit()

        list_weekday = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche']

        if cmd == 'add':

            args = args[1:]
            weekday = ''
            index = 0

            try:

                while args[index].capitalize() not in list_weekday:

                    index += 1
                    weekday = args[index].capitalize()              
                
                name = ''.join(item + " " for item in args[:index])
                args = args[index + 1:]

                try:
                    hour = ''.join(item for item in args)

                except:
                    hour = "Heure inconnue"

                r = re.compile('^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$')

                if r.match(hour) is None:
                    hour = "Heure inconnue"

            except:
                self.client.say("Il manque des informations.")


            weekday.capitalize()

            query = "INSERT INTO AL (name, day, hour) VALUES ('" + name + "', '" + weekday + "', '" + hour + "')"

            db_cursor.execute(query)
            db_co.commit()

            await self.client.say("Fait")


        if cmd == 'v' or cmd == 'view':

            now = datetime.datetime.now()
            day = now.weekday()

            for i in range (0, 7):

                if i == day:
                    day = list_weekday[i]

            query = 'SELECT name FROM AL '
            db_cursor.execute(query)

            list_name = []
            db_output = db_cursor.fetchall()

            for item in db_output:

                item = str(item)
                item = item.replace("'", '')
                item = item.replace('"', '')
                item = item.replace("(", '')
                item = item.replace(")", '')
                item = item.replace("[", '')
                item = item.replace("]", '')
                item = item.replace(",", '')
                item = item.replace("_", ' ')

                list_name.append(item)

            query = 'SELECT hour FROM AL'
            db_cursor.execute(query)

            list_hour = []
            db_output = db_cursor.fetchall()

            for item in db_output:

                item = str(item)
                item = item.replace("'", '')
                item = item.replace('"', '')
                item = item.replace("(", '')
                item = item.replace(")", '')
                item = item.replace("[", '')
                item = item.replace("]", '')
                item = item.replace(",", '')

                list_hour.append(item)

            query = 'SELECT day FROM AL'
            db_cursor.execute(query)

            list_day = []
            db_output = db_cursor.fetchall()

            for item in db_output:

                item = str(item)
                item = item.replace("'", '')
                item = item.replace('"', '')
                item = item.replace("(", '')
                item = item.replace(")", '')
                item = item.replace("[", '')
                item = item.replace("]", '')
                item = item.replace(",", '')

                list_day.append(item)

            else:
                try:
                    p = int(args[1])

                except:
                    p = 1

            min = int(p * 10 - 10)
            max = int(p* 10)

            if len(list_name) % 10 == 0:
                pmax = int(len(list_name) / 10)

            else:
                pmax = int(len(list_name) / 10) + 1

            list_name = list_name[min: max]
            list_day = list_day[min: max]
            list_hour = list_hour[min: max]

            embed = discord.Embed(title = "Anime qui sortent aujourd'hui", description = "List d'animes de toute la semaine")

            for weekday in list_weekday:

                list_day_display = []
                list_hour_display = []
                list_name_display = []

                for i in range (0, len(list_name)):

                    if weekday == list_day[i]:
                        list_day_display.append(list_day[i])
                        list_hour_display.append(list_hour[i])
                        list_name_display.append(list_name[i])

                list_hour_in_minute = []
                list_hour_formated = []
                list_index_unsorted = []

                for item in list_hour_display:
                    
                    try:
                        mh = int(item[0:2]) * 60
                        mt = mh + int(item[3:5])

                        list_hour_in_minute.append(mt)
                    
                    except:
                        None
                        
                list_index_unsorted = [i for i, e in enumerate(list_hour_display) if e == 'Heure inconnue']

                list_hour_in_minute.sort()

                for item in list_hour_in_minute:

                    h = int(item / 60)
                    m = item - (h * 60)

                    if len(str(h)) != 2:
                        h = '0' + str(h)

                    f = str(h) + ':' + str(m)

                    list_hour_formated.append(f)

                for item in list_hour_formated:
                    index = list_hour_display.index(item)

                    embed.add_field(name = list_name_display[index], value = list_day_display[index] + " à " + list_hour_display[index], inline = False)

                try:
                    for idx in list_index_unsorted:
                        embed.add_field(name = list_name_display[idx], value = list_day_display[idx] + " à " + list_hour_display[idx], inline = False)

                except:
                    None

            embed.set_footer(text = 'Page : ' + str(p) + ' / ' + str(pmax))

            await self.client.say(embed = embed)


        if cmd == 'r' or cmd == 'remove':

            try :
                name = ''.join(item + ' ' for item in args[1:])

            except:
                await self.client.say("Aucun nom donné")
                return

            query = 'DELETE FROM AL WHERE name = "' + name + '"'
            db_cursor.execute(query)
            db_co.commit()

            await self.client.say("Fait")           

        
        if cmd == 'e' or cmd == 'edit':

            query = 'SELECT name FROM AL'
            db_cursor.execute(query)
            db_output = db_cursor.fetchall()

            list_name = []

            for item in db_output:

                item = str(item)
                item = item.replace("'", '')
                item = item.replace('"', '')
                item = item.replace("(", '')
                item = item.replace(")", '')
                item = item.replace("[", '')
                item = item.replace("]", '')
                item = item.replace(",", '')
                item = item.replace("_", '')

                list_name.append(item)

            name = ''
            index = 0

            for i in range (0, len(args)):

                name = "".join(item + " " for item in args[1:i])
                print (name)

                if name in list_name :
                    index = i
                    break

            print(name, index)
            print (args[index + 1])

            try:

                item_to_update = ''
                item_to_update = "".join(item + " " for item in args[index + 1:])

                if args[index] == 'name':

                    query = 'UPDATE AL SET name = "' + item_to_update + '" WHERE name = "' + name + '"'
                    db_cursor.execute(query)
                    db_co.commit()

                    await self.client.say("Fait")

                if args[index] == 'day':

                    item_to_update = item_to_update.replace(' ', '')

                    if item_to_update in list_weekday:

                        query = 'UPDATE AL SET day = "' + item_to_update + '" WHERE name = "' + name + '"'
                        db_cursor.execute(query)
                        db_co.commit()

                        await self.client.say("Fait")

                    else:

                        await self.client.say('Jour incorrect')

                if args[index] == 'hour':

                    item_to_update = item_to_update.replace(" ", '')

                    r = re.compile('^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$')

                    if r.match(item_to_update) is None:

                        item_to_update = "Heure inconnue"
                        query = 'UPDATE AL SET hour = "' + item_to_update + '" WHERE name = "' + name + '"'
                        db_cursor.execute(query)
                        db_co.commit()

                        await self.client.say("Fait")

                    else:
                        query = 'UPDATE AL SET hour = "' + item_to_update + '" WHERE name = "' + name + '"'
                        db_cursor.execute(query)
                        db_co.commit()

                        await self.client.say("Fait")

                if args[index] != 'hour' and args[index] != 'day' and args[index] != 'name':

                    await self.client.say("Argument incorrect")

            except Exception as e:
                print(e)
                await self.client.say("Erreur")   


        if cmd == 'help' or cmd == 'h':

            embed = discord.Embed(title = '!k AL_conf', description = "liste des commandes pour config l'alerte anime")

            embed.add_field(name = '!k AL_conf add { nom } { jour } optionnelle { heure }', value = 'Ajoute un anime à la liste', inline = False)
            embed.add_field(name = '!k AL_conf v ', value = 'Voir les animes du jour', inline = False)
            embed.add_field(name = '!k AL_conf r ou !k AL_conf remove { nom }', value = 'Supprimer un anime de la liste', inline = False)
            embed.add_field(name = '!k AL_conf e ou !k AL_conf e { nom } + |name|day|hour| + { nouvelle valeur }', value = "Changer UNE seule valeur d'un anime de la liste")

            await self.client.say(embed = embed)


        if cmd == 'set':

            try:
                boolean = args[1]
                boolean.capitalize()

            except:
                await self.client.say('Il faut un argument : True ou False')

            if boolean != 'True' and boolean != 'False':

                await self.client.say('Il faut un argument : True ou False')

            query = 'SELECT server FROM AL_server WHERE server = "' + ctx.message.server.id + '"'
            db_cursor.execute(query)

            db_output = db_cursor.fetchall()

            for i in range(0, len(db_output)):
                
                if ctx.message.server.id not in db_output[i]:

                    await self.client.say("Bizarre le server n'as pas été trouvé")

                else:
                    query = 'UPDATE AL_server SET as_to_be_display = "' + boolean + '" WHERE server = "' + ctx.message.server.id + '"'
                    db_cursor.execute(query)
                    db_co.commit()

                    await self.client.say("Fait")


def setup (client):
    client.add_cog(AL(client))