import datetime
import discord
from discord.ext.commands import Bot
import mysql.connector
from mysql.connector import errorcode


client = Bot(command_prefix = '!k ') 
TOKEN = 'NDY2NTI3Mjk3MTgxMzE5MTY4.DifsGg.krSYtJPgjo8wTma09-Qm7YREmdc'

@client.event
async def on_ready():

	db_co = mysql.connector.connect(user= 'Arcarox_querry', host = '86.74.198.79', port = '3306', password = 'arcarox', database = 'klein_bot')
	db_cursor = db_co.cursor()

	query = '''CREATE TABLE IF NOT EXISTS AL (
            	id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
            	name VARCHAR(255) NOT NULL,
            	day VARCHAR(10) NOT NULL,
            	hour CHAR(5) NOT NULL
            	);'''

	db_cursor.execute(query)
	db_co.commit()

	now = datetime.datetime.now()
	day = now.weekday()

	list_weekday = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche']

	for i in range (0, 7):

    		if i == day:
        		day = list_weekday[i]

	query = 'SELECT name FROM AL WHERE day = "' + day + '"'
	db_cursor.execute(query)

	list_name = []
	db_output = db_cursor.fetchall()

	for item in db_output:

		item = str(item)
		item = item.replace("'", '')
		item = item.replace('"', '')
		item = item.replace("(", '')
		item = item.replace(")", '')
		item = item.replace("[", '')
		item = item.replace("]", '')
		item = item.replace(",", '')
		item = item.replace("_", ' ')

		list_name.append(item)

	query = 'SELECT hour FROM AL WHERE day = "' + day + '"'
	db_cursor.execute(query)

	list_hour = []
	db_output = db_cursor.fetchall()

	for item in db_output:

    		item = str(item)
    		item = item.replace("'", '')
    		item = item.replace('"', '')
    		item = item.replace("(", '')
    		item = item.replace(")", '')
    		item = item.replace("[", '')
    		item = item.replace("]", '')
    		item = item.replace(",", '')

    		list_hour.append(item)


	embed = discord.Embed(title = "Anime qui sortent aujourd'hui", description = "List d'animes du " + day)

	list_hour_in_minute = []
	list_hour_formated = []
	list_index_unsorted = []

	for item in list_hour:

		try:
			mh = int(item[0:2]) * 60
			mt = mh + int(item[3:5])

			list_hour_in_minute.append(mt)

		except:
			None

	list_index_unsorted = [ i for i, e in enumerate(list_hour) if e == 'Heure inconnue' ]

	list_hour_in_minute.sort()

	for item in list_hour_in_minute:

		h = int(item / 60)
		m = item - (h * 60)

		if len(str(h)) != 2:
			h = '0' + str(h)

		f = str(h) + ':' + str(m)

		list_hour_formated.append(f)

	for item in list_hour_formated:
		index = list_hour.index(item)

		embed.add_field(name = list_name[index], value = list_hour[index], inline = False)

	try:
		for index in list_index_unsorted:
			embed.add_field(name = list_name[index], value = list_hour[index], inline = False)

	except:
		None

	channs = client.get_all_channels()

	query = 'SELECT server FROM AL_server WHERE as_to_be_display = "True"'
	db_cursor.execute(query)
	db_output = db_cursor.fetchall()

	list_server = []

	for item in db_output:

		item = str(item)
		item = item.replace("'", '')
		item = item.replace('"', '')
		item = item.replace("(", '')
		item = item.replace(")", '')
		item = item.replace("[", '')
		item = item.replace("]", '')
		item = item.replace(",", '')

		list_server.append(item)

	for chann in channs:

		if chann.name == 'alerte_anime' and chann.server.id in list_server:
			await client.send_message(chann, embed = embed)

	await client.close()

client.run(TOKEN)
