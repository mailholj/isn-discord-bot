import discord
from discord.ext import commands
import mysql.connector
from mysql.connector import errorcode
import datetime


class NF():

    def __init__(self, client):
        self.client = client

    @commands.command(pass_context = True)
    async def nf(self, ctx, *args):

        try:
            db_co = mysql.connector.connect(user = 'Arcarox_querry', host = '86.74.198.79', port = '3306', password = 'arcarox', database = 'klein_bot')
            db_cursor = db_co.cursor()

        except mysql.connector.Error as err :

            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Something is wrong with your user name or password")

            elif err.errno == errorcode.ER_BAD_DB_ERROR:

                print("Database does not exist")

            else:
                print(err)

        query = '''CREATE TABLE IF NOT EXISTS nf (
            id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
            user VARCHAR(255) NOT NULL,
            sentence TEXT NOT NULL,
            date CHAR(16) NOT NULL
            );'''

        db_cursor.execute(query)
        db_co.commit()


        cmd = args[0]

        if cmd == 'add':

            now = datetime.datetime.now()
            date = now.strftime("%Y-%m-%d %H:%M")

            try:
                user = str(ctx.message.mentions[0])

            except:
                await self.client.say("Personne n'est mentionnée")
                return

            stc = ''

            try:
                stc = args[2]
                for i in range (3, len(args)):

                    stc = stc + args[i] + " "

            except:
                await self.client.say("Aucune phrase trouvé")
                return

            query = 'INSERT INTO nf (user, sentence, date) VALUES ("' + user + '", "' + stc + '", "' + date + '")'
            db_cursor.execute(query)
            db_co.commit()
            await self.client.say("Fait")

        if cmd == 'v' or cmd == 'view':

            try:
                page = int(args[1])

            except:

                try:
                    user = str(ctx.message.mentions[0])

                    try:
                        page = int(args[2])

                    except:
                        page = 1

                except:
                    user = ""
                    page = 1

            if user != '':

                query = 'SELECT sentence FROM nf WHERE user = "' + user + '"'
                db_cursor.execute(query)
                stc_db_output = db_cursor.fetchall()

                query = 'SELECT date FROM nf WHERE user = "' + user + '"'
                db_cursor.execute(query)
                date_db_output = db_cursor.fetchall()

            else:
                query = 'SELECT user FROM nf'
                db_cursor.execute(query)
                user_db_output = db_cursor.fetchall()

                query = 'SELECT sentence FROM nf'
                db_cursor.execute(query)
                stc_db_output = db_cursor.fetchall()

                query = 'SELECT date FROM nf'
                db_cursor.execute(query)
                date_db_output = db_cursor.fetchall()

            list_user = []
            list_stc = []
            list_date = []


            for i in range (0, len(stc_db_output)):

                item = str(stc_db_output[i])

                item = item.replace(",",'')
                item = item.replace("(",'')
                item = item.replace(")",'')
                item = item.replace("[",'')
                item = item.replace("]",'')
                item = item.replace("'",'')
                item = item.replace('"','')

                list_stc.append(item)

            for i in range (0, len(date_db_output)):

                item = str(date_db_output[i])

                item = item.replace(",",'')
                item = item.replace("(",'')
                item = item.replace(")",'')
                item = item.replace("[",'')
                item = item.replace("]",'')
                item = item.replace("'",'')
                item = item.replace('"','')

                list_date.append(item)

            if user == '':
                
                for i in range (0, len(user_db_output)):
                    
                    item = str(user_db_output[i])

                    item = item.replace(",",'')
                    item = item.replace("(",'')
                    item = item.replace(")",'')
                    item = item.replace("[",'')
                    item = item.replace("]",'')
                    item = item.replace("'",'')
                    item = item.replace('"','')

                    list_user.append(item)


            min = int(page * 10 - 10)
            max = int(page * 10)

            if len(list_stc) % 10 == 0:
                pmax = int(len(list_stc) / 10)

            else:
                pmax = int(len(list_stc) / 10) + 1

            list_date = list_date[min:max]
            list_stc = list_stc[min:max]
            list_user = list_user[min:max]

            embed = discord.Embed(title = 'Phrase à ne pas oublier', description = "Liste de phrase qui n'aurait jamais qui n'aurait pas dû être dites")

            if user == '':

                for i in range (0, len(list_user)):

                    embed.add_field(name = list_stc[i], value = 'user: ' + list_user[i] + '   /  date: ' + list_date[i], inline = False)

            else:
                for i in range (0, len(list_stc)):
                    embed.add_field(name = list_stc[i], value = 'user: ' + user + '   /  date: ' + list_date[i], inline= False)
            

            embed.set_footer(text = 'Page : ' + str(page) + ' / ' + str(pmax))
            await self.client.say(embed = embed)
        
        if cmd == 'h' or cmd == 'help':

            embed = discord.Embed(title = 'help pour nf', description = 'commande possible pour nf')

            embed.add_field(name = '!k nf add @{mentino la persone concernée} {phrase}', value = 'Ajouter une phrase')
            embed.add_field(name = '!k nf v + optional @{mention une personne précise}', value = "Voir les phrases de tout les mondes ou seulement d'une seul personne")

        
        if cmd == 'r' or cmd == 'remove':

            if ctx.message.author.id == "228966738358697986":

                try : 
                    stc = args[1:]

                except:
                    await self.client.say("Aucune phrase entrée")

                query = 'DELETE * FROM nf WHERE sentence = "' + stc + '"'
                db_cursor.execute(query)
                db_co.commit()

                await self.client.say("Fait")
            
            else:
                await self.client.say("Tu n'as pas les autorisations pour faire cela.")


def setup(client):
    client.add_cog(NF(client))