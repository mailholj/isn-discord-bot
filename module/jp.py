import discord
from discord.ext import commands

class JP():

	def __init__ (self, client):
		self.client = client

	@commands.command()
	async def jp(self, *args):
		cmd = args[0]

		global nbr_propose
		global nbr_a_trouver
		global nbr_try

		from random import randint

		if cmd == '' :

			say = "Bonjour vous êtes la pour jouer au juste prix !\nFaites !k juste_prix start pour jouer, l'intervalle du jeu est de 0 à 100"
			await self.client.say(say)

		if cmd == 'start' or cmd == 'Start' :

			nbr_a_trouver = randint(1, 101)
			say = "Vous pouvez cherchez le juste prix, entrer !k jp try (votre_nombre)." 
			nbr_try = 0
			await self.client.say(say)

		if cmd == 'try' :

			nbr_propose = args[1]
			nbr_propose = int(nbr_propose)

			if nbr_propose > nbr_a_trouver :
				say = "C'est moins (" + str(nbr_propose) + ")"
				nbr_try = nbr_try + 1
				await self.client.say(say)

			if nbr_propose < nbr_a_trouver :
				say = "C'est plus (" + str(nbr_propose) + ")"
				nbr_try = nbr_try + 1
				await self.client.say(say)

			if nbr_propose == nbr_a_trouver :
				say = "Bravo c'est gagné, le nombre a trouvé était " + str(nbr_a_trouver) + " .\nVous avez trouvé en " + str(nbr_try) + " coups."
				await self.client.say(say)

def setup(client):
    client.add_cog(JP(client))