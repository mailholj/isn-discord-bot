import discord
from discord.ext import commands
import subprocess
import atexit

class git_update():

    def __init__ (self, client):
        self.client = client

    @commands.command(pass_context = True)
    async def gupdate (self, ctx):

        if ctx.message.author.id == "228966738358697986":
            completed = subprocess.run('sudo git pull', shell = True)
            print('returncode:', completed.returncode)
            await self.client.logout()


def setup (client):
    client.add_cog(git_update(client))
