import discord
from discord.ext import commands
import mysql.connector
from mysql.connector import errorcode

class TDL():

    def __init__ (self, client):
        self.client = client

    @commands.command(pass_context = True)
    async def tdl(self, ctx, *args) :

        try:
            db_co = mysql.connector.connect(user= 'Arcarox_querry', host = '86.74.198.79', port = '3306', password = 'arcarox', database = 'klein_bot')
            db_cursor = db_co.cursor()

        except mysql.connector.Error as err :

            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Something is wrong with your user name or password")

            elif err.errno == errorcode.ER_BAD_DB_ERROR:

                print("Database does not exist")

            else:
                print(err)

        query = '''CREATE TABLE IF NOT EXISTS tdl (
            id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
            user VARCHAR(255) NOT NULL,
            title VARCHAR(255) NOT NULL,
            description TEXT
            );'''

        db_cursor.execute(query)
        db_co.commit()

        cmd = args[0]

        if cmd == 'v' :

            query = "SELECT title FROM tdl WHERE user = '" + ctx.message.author.id + "'"
            db_cursor = db_co.cursor()
            db_cursor.execute(query)
            db_output_title = db_cursor.fetchall()
            user = ctx.message.author.id

            query = "SELECT description FROM tdl WHERE user = '" + user + "'"
            db_cursor = db_co.cursor()
            db_cursor.execute(query)
            db_output_description = db_cursor.fetchall()

            if db_output_title == '' :

                await self.client.say("Nothing")
                return

            title = []
            description = []

            for i in range (0, len(db_output_title)):

                item = str(db_output_title[i])

                item = item.replace(",",'')
                item = item.replace("(",'')
                item = item.replace(")",'')
                item = item.replace("[",'')
                item = item.replace("]",'')
                item = item.replace("'",'')
                item = item.replace('"','')

                title.append(item)

            for i in range (0, len(db_output_description)):

                item = str(db_output_description[i])

                item = item.replace(",",'')
                item = item.replace("(",'')
                item = item.replace(")",'')
                item = item.replace("[",'')
                item = item.replace("]",'')
                item = item.replace("'",'')
                item = item.replace('"','')

                description.append(item)

            try:
                p = int(args[1])
                if p == 0 :
                    p = 1
            except:
                p = 1

            MIN = (p * 10) - 10
            MAX = MIN + 10

            if (len(title) % 10) == 0:

                pMAX = int(len(title) / 10)
            else:
                pMAX = int(len(title) / 10) + 1

            if MIN > len(title):
                MIN = 0
                MAX = 10

            embed = discord.Embed(title = 'Ta to-do-list :', description = 'page :' + str(p) + '/' + str(pMAX), inline = False)

            title = title[MIN: MAX]
            description = description[MIN: MAX]

            for i in range (0, len(title)) :

                if description[i] != '':

                    embed.add_field(name = title[i], value = description[i], inline = False)
                else:
                    embed.add_field(name = title[i], value = 'pas de description', inline = False)


            await self.client.say(embed = embed)


        if cmd == 'add':

            try:
                title = str(args[1])

            except:
                await self.client.say('Absence de nom')

            description = ''

            for i in range (2, len(args)):
                description = description + args[i] + " "


            user = ctx.message.author.id

            query = 'INSERT INTO tdl (user, title, description) VALUES ("' + user + '", "' + title + '", "' + description + '")'
            db_cursor.execute(query)
            db_co.commit()
            db_cursor.close()
            db_co.close
            await self.client.say("Fait")


        if cmd == 'r' or cmd == 'remove':

            title = args[1]
            query = 'DELETE FROM tdl WHERE title = "' + title + '"'
            db_cursor.execute(query)
            db_co.commit()
            await self.client.say("Fait")

        if cmd == "help" or cmd == "h":

                embed = discord.Embed(title = 'help pour tdl', descritpion = 'commande possible pour tdl', inline = False)
                embed.add_field(name= "!k tdl v ou !k tdl view ", value = "list des éléments de la tdl", inline = False)
                embed.add_field(name= "!k tdl add {titre de la tdl} {description}", value = "Ajoute un élément à la tdl", inline = False)
                embed.add_field(name= "!k tdl remove ou  !k tdl r  {titre de la tdl}", value= "Supprime un élément de la tdl", inline = False)

                await self.client.say(embed = embed)

        db_co.close()

def setup(client):
    client.add_cog(TDL(client))
